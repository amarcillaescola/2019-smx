# Configuració de l’entorn de treball

Abans de res cal conèixer i configurar l’entorn de treball, eines de
comunicació, etc.

## Descripció

Aquesta activitat explora tres entorns:

* Gestor d’escriptori _GNome_.
* Correu electrònic.
* Full de càlcul de _GSheets_ per mantenir registre d’activitats.

Seguint la màxima “eat your own dog food” usarem la llista de
correu del grup per tot el que sigui necessari, des de el primer dia del curs:
lliurar exercicis, documents, plantejar dubtes, etc. Podem prefixar sempre el
tema del missatge amb l’identificador de l’activitat, com ara
`[MP3UF1RA1A2]`, etiquetar apropiadament els missatges en la web del grup de
correu, etc.

Cal afegir que disposar de paper i llapis o similar s’ha de considerar un _entorn_
de treball necessari i obligatori ;-)

## Documentació

Per accedir a determinats serveis de _Google_ farà falta disposar d’un compte
de correu de _GMail_.

## Pràctiques

1. Explora el gestor d’escriptori _GNome_:
	- Compte d’usuari. Inici de sessió.
	- Gestió de finestres.
		* Per canviar de finestra amb el teclat usa les tecles `ALT-TAB`.
	- Tecles per dimensionar finestres.
	- Configuració de les dreceres de teclat de _GNome_.
		* Assigna a la combinació de tecles `SUPER-F11` l’acció **Toggle full screen mode**.
		* Saps quines tecles permeten moure finestres entre escritoris
		  virtuals del _Gnome_? I com canviar d’escriptori sense usar el ratolí?
	- Execució d’aplicacions.
	- Ajuda de _GNome_ amb _Yelp_.
	- Us del navegador _Firefox_.

2. Si cal crea per a tu un compte de correu, per exemple amb <http://gmail.com>.

3. Si cal configura el teu compte de correu per el remitent sigui clar i
   correcte.

4. Subscriu-te via Web al grup (llista de correu) que et correspongui:
	- <https://tinyurl.com/1hism2019>
	- <https://tinyurl.com/1jism2019>

5. Escriu el teu missatge de presentació al teu grup, ara des del client de correu:
	- hism1_2019-grup@correu.escoladeltreball.org
	- jism1_2019-grup@correu.escoladeltreball.org
	- Una vegada tothom estigui subscrit rebrà aquest mateix document
	  en un nou missatge del professor amb el prefix `[MP3UF0]` en el tema.
	  Verifica-ho.

6. Explora la interfície Web del grup:
	- <https://tinyurl.com/1hism2019>
	- <https://tinyurl.com/1jism2019>

7. Accedeix amb _Firefox_ a [GSheets](http://sheets.google.com/), utilitzant el
   teu compte de _Google_, per crear un registre personal activitats:
	- Usarem un nou full de càlcul amb tan sols dues columnes.
	- Selecciona la columna **A** i aplica un format de data.
	- Escriu la data actual en la cel·la **A1** amb el format `YYYY-MM-DD`.
	- En la cel·la **A2** escriu la formula `=A1+1`.
	- Copia la cel·la **A2** en la cel·la **A3**.
	- En la columna **B** escriu les teves anotacions. Pots començar amb la
	  corresponent al dia d’avui.
	- Opcional:
		- Elimina les columnes de la **C** fins al final.
		- Afegeix primera fila amb noms per les columnes: `DATA` i
		  `COMENTARI`.

8. Recorda que és obligatori en aquesta assignatura mantenir al dia aquest
   registre d’activitats, amb una fila per cada dia de classe.

