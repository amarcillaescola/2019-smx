# Usar fulls de càlcul com taules: organització, consultes

Introducció a la gestió de taules de dades.

## Descripció

Començarem treballant les expressions relacionals i lògiques, ara dins dels
fulls de càlcul. A continuació aprendrem a preparar correctament les taules de
dades i veurem com ordenar o filtrar aquesta informació

## Documentació

Normes per preparar _taules_ ben formades:

* Mai usar columnes buides.
* Mai usar files buides.
* Etiquetar columnes en la primera fila.
* Congelar la primera fila.
* En cada cel·la posar sempre un sol valor (mai per exemple usar cadenes amb
  subvalors separats per comes).
* Assignar _tipus_ (i format) a les cel·les de forma anticipada.
* Decidir com representar valors nuls.
* Mai amagar files o columnes.
* En cada full hi haurà sempre una sola taula.

A tenir en compte en la gestió de _columnes calculades_:

1. Posar la formula inicial en la primera cel·la de la columna.
2. Copiar la formula en la resta de cel·les de la columna:
	1. Copiar la cel·la _font_,
	2. seleccionar cel·les  _destí_,
	3. enganxar la formula en totes les cel·les seleccionades.
3. En afegir files noves cal copiar la formula en a nova fila.
4. Si modifiquem la definició de la formula cal tornar-la a copiar en totes les
   cel·les de la columna.

## Pràctiques sobres expressions relacionals i lògiques amb _Calc_

Per filtrar files en les taules ens caldrà usar expressions relacionals i
lògiques.

1. Verifica amb **nombres**, practicant en un full de càlcul, els resultats
generats per aquests operadors (veus les diferències amb _Python_?):
	- `=`
	- `<>`
	- `>`
	- `<`
	- `>=`
	- `<=`

2. Ara verifica amb **cadenes** de text els mateixos operadors. Com va amb les
   lletres accentuades? Per exemple, quin resultat genera l’expressió `"e" < "à"`.

3. Quines funcions lògiques ens ofereix el full de càlcul? Obre l’_Auxiliar de
   funcions_ (`CTRL-F2`) i explora la documentació d’aquesta categoria de funcions.

4. Prepara un full de càlcul amb exemples d’us de les funcions lògiques més
   comuns. En particular, no oblidis la funció `IF` del full de càlcul, que és
   equivalent a l’expressió condicional de _Python_ però amb una sintaxi
   diferent.

5. Com podem saber si una cel·la està buida? Disposem de diferents alternatives
(verifica-les):
	- La funció `ISBLANK(cell)`.
	- Comparar amb la cadena buida: `cell=""`
	- Per valors numèrics i si no ens importa no diferenciar entre cel·les
	  buides i zero: `cell=0`.

## Pràctiques sobre la gestió de taules de dades

Una vegada hem organitzat correctament una taula és molt simple ordenar o
filtrar el seu contingut.

1. Prepara correctament la taula del fitxer [AMBITS].
	- Desa’l com a fitxer ODS. Treballa ara amb aquesta còpia.
	- Elimina files i columnes, innecessàries, etc.
	- Ara ja pots afegir columnes calculades: suma de tots els anys,
	  comptar anys amb valors superiors o inferiors a un determinat valor,
	  etc.
2. Explora les possibilitats que ofereix el full de càlcul per ordenar la
   taula.
	- És aquesta ordenació _destructiva_? És a dir, pots recuperar
	  l’ordenació original una vegada desat el fitxer?
	- Per poder recuperar l’ordre inicial et cal una columna numerada.
	  Prepara-la amb _auto-fill_.
3. Explora les possibilitats que ofereix el full de càlcul per filtrar la
   taula.  Són els filtres _destructius_? 


[COMENTARI]: <#> "Base de dades de links usats en aquest document."
[AMBITS]: <http://habitatge.gencat.cat/web/.content/home/dades/estadistiques/01_Estadistiques_de_construccio_i_mercat_immobiliari/03_Mercat_de_lloguer/02_Lloguers_per_ambits_geografics/Fitxers_contractes/ambits_anual_contractes.xls> "ambits_anual_contractes.xls"

