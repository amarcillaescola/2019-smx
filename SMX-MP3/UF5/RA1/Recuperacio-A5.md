# Fer consultes amb un SGBDR: taules, consultes QBE, camps calculats

Les consultes constitueixen el nucli essencial dels sistemes gestors de bases
de dades, i a elles dedicarem la major part del temps dedicat a aquest tema.

Per fer les consultes usarem l’entorn
[QBE](https://ca.wikipedia.org/wiki/Cerca_mitjan%C3%A7ant_exemple) que
proporciona _Libre Office Base_.

## Documentació

La documentació interactiva de _Libre Office Base_ també hi és a la Web:

- [Base Help](https://help.libreoffice.org/6.3/en-US/text/shared/explorer/database/main.html)

En l’ajuda interactiva de _Libre Office Base_ consulta la secció **Working with
Queries**, i especialment l’ajuda sobre el **Query Wizard** i la **Query Design View**.

També farà falta consultar les funcions predefinides que podem usar en les consultes:

- [HyperSQL Funcions](http://hsqldb.org/doc/guide/guide.html#builtinfunctions-chapt)

## Pràctiques I

Farem totes les pràctiques amb una mateixa base de dades.

1. Descarrega aquest [fitxer ZIP](https://ioc.xtec.cat/materials/FP/Materials/2201_SMX/SMX_2201_M03/web/html/WebContent/u5/media/base3.zip).
2. Extreu el seu contingut i desa’l en el teu directori de treball. Conserva el
   fitxer ZIP per si en el futur et cal recuperar l’estat inicial de la base de dades.
3. Familiaritzat amb les taules: el seu nom, les columnes que tenen i els seus noms, en
   quina columna hi ha la clau principal,  quantitat de files, etc.
4. Explora les relacions definides en la base de dades (menú **Tools**).
	1. En l’editor de relacions explora el menú que defineix cada relació
	   (clica cada línia i usa el menú contextual, o be fes _doble clic_).
	2. Totes les taules tenen una clau principal, però quines taules tenen
	   més d’una clau? Quines tenen claus foranies, es a dir, que enllacen
	   amb la clau principal d’una altre taula.
5. Consulta la secció **Query Design View** en l’ajuda de _Libre Office Base_.
   Mira de tenir sempre aquesta ajuda a la vista.
6. Estudia aquest
   [tutorial](https://ioc.xtec.cat/materials/FP/Materials/2201_SMX/SMX_2201_M03/web/html/WebContent/u5/a3/continguts.html)
   i fes les seves [pràctiques](https://ioc.xtec.cat/materials/FP/Materials/2201_SMX/SMX_2201_M03/web/html/WebContent/u5/a3/activitats.html).
   No oblidis de fer els [exercicis d’autoavaluació](https://ioc.xtec.cat/materials/FP/Materials/2201_SMX/SMX_2201_M03/web/html/WebContent/u5/a3/exercicis.html).

## Pràctiques II

Fes aquestes noves consultes a la base de dades, ja sigui amb el **Query
Wizard** o directament en la finestra **Query Design View**. Posa un nom
apropiat a cada consulta i conserva-la per la seva revisió futura. No deixis de
tenir a la vista l’ajuda sobre la **Query Design View**.

Per curiositat pots comparar de tant en tant el que fas en l’entorn
[QBE](https://ca.wikipedia.org/wiki/Cerca_mitjan%C3%A7ant_exemple) i
l’expressió [SQL](https://ca.wikipedia.org/wiki/Structured_Query_Language)
equivalent que pots visualitzar amb l’opció **Switch Design View On/Off** del
menú **View** o be el botó corresponent

1. _Productes_ ordenats per nom.
2. _Productes_ amb un preu inferior a `10`.
3. _Productes_ de la categoria `Mongeta`.
4. _Productes_ vegetals.
5. _Categories_ de productes càrnics.
6. _Productes_ mostrant el nom de la categoria i no el seu identificador.
7. _Clients_ de poblacio Reus.
8. _Clients_ de la província de Girona.
9. _Clients_ amb `Mail`.
10. _Clients_ amb `Observacions`.
11. _Clients_ més grans de 40 anys.
12. _Comandes_ mostrant el nom dels clients i ordenades per cognom.
13. _Comandes_ no lliurades.
14. _Comandes_, amb els seus detalls, del client _Isabel Valiente Toro_.
15. _Clients_ amb _Comandes_.
16. _Clients_ sense _Comandes_.
17. _Productes_ que ningú ha comprat.
18. _Productes_ més venuts.
19. Primera/última comanda rebuda.
20. Dissenya la relació **Universal**, ben ordenada per facilitar la lectura, amb
    tots els clients, les seves comandes, els detalls de les mateixes, etc.
    Elimina de la vista els camps _ID_ que puguis i mostra tan sols els noms
    del productes, categories, etc.  No t’ha de preocupar que aquesta vista
    tingui moltes files.


