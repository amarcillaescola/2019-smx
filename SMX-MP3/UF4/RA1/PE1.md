# Full de càlcul: conceptes bàsics

## Descripció

Es proporcionarà als alumnes un fitxer ODS on, en diferents fulls, es
presenten exercicis pràctics i escrits per ser resolts.

Una vegada completats els exercicis els alumnes han de lliurar el fitxer ODS
modificat en la forma que s’els indicarà.

