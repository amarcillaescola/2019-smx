# Correu i llistes: conceptes bàsics

Donades les característiques d’aquesta unitat formativa ens limitarem a
verificar, per avaluar-la, que els alumnes saben crear de forma completament
correcte missatges de correu.

## Exercicis

Redacta, en un simple missatge de text, una breu descripció del format dels
missatges SMTP. Una vegada completat i corregit el missatge l’envies a l’adreça
del professor.

**Important**: aquest exercici és extremadament fàcil, raó per la qual
qualsevol error present en el missatge baixarà significativament la nota.

