# Configuració del client de correu

Per gestionar de forma professional el correu personal necessitem:

+ Respectar la [_netiquette_](https://tools.ietf.org/html/rfc1855),
+ configurar una signatura apropiada pels nostres missatges,
+ classificar els missatges amb l’ajuda de filtres i etiquetes o carpetes,
+ gestionar l’agenda de contactes
+ i crear llistes de distribució personals.

## Configurar signatura dels missatges

Tradicionalment els usuaris de correu finalitzen tots els seus missatges amb un
text que els representa. És molt important configurar aquesta signatura en els
vostres dispositius mòbils, que moltes vegades venen per defecte amb una
signatura publicitària.

* <https://en.wikipedia.org/wiki/Signature_block>
* <https://duckduckgo.com/?q=ascii+art+email+signatures>

## Classificar els missatges de correu

Cal que la safata d’entrada dels missatges contingui únicament els missatges
pendents de gestionar. Tots els missatges ja gestionats han de ser arxivats en
carpetes separades segons la seva temàtica. Organitza ara el teu correu si
encara no tens el costum de fer-ho, i prepara filtres per que els nous
missatges rebuts, si és convenient, és classifiquin automàticament quan
arribin.

## Gestionar l’agenda de contactes

Configura apropiadament la teva agenda de contactes. Gestió segons GMail:

- <https://contacts.google.com/>

1. Accedeix als contactes i deixa visible l’ajuda (darrera opció del menú: això
   pot variar…).
2. Afegeix contactes per tots els teus professors; edita correctament
   l’informació.

## Crear llistes de correu personals

Tots els programes clients de correu permeten crear _alies_ per us personal.
Aquests _alies_ ho poden ser d’una sola persona o, el més comú, d’un grup.
Prepara en el teu client de correu llistes personals per diferents grups d’usuaris.

1. Fes una llista (amb una etiqueta) per uns quants companys de classe.
2. Fes una llista (amb una etiqueta) un grup que contingui tots els teus
   professors.

## Configurar clients de correu locals

1. Configura el programa _Evolution_ per accedir al correu de GMail amb el
   protocol [IMAP](https://support.google.com/mail/answer/7126229?hl=ca).
2. Alternativament, configura a _Evolution_ altres servidors de correu dels que
   tinguis comptes.
3. Configura el programa _Evolution_ per accedir al correu de GMail amb el
   protocol [POP3](https://support.google.com/mail/answer/7104828?hl=ca).
   Important: vigila de **no descarregar** el correu! Volem que quedi còpia en
   el servidor.

## Usar altres proveïdors de correu alternatius

1. Registra't en aquests servidors de correu alternatius a GMail:
    + <http://protonmail.com>
    + <http://tutanota.com>
2. Configura el teu compte de GMail per reenviar els missatges cap a
   _ProtonMail_ o _TutaNota_. Conserva còpia dels missatges!
3. Configura _vacation_ (el missatge de resposta automàtic per quan estas de
   vacances) en el teu compte de GMail per informar als teus remitents de que
   migraràs aviat a _ProtonMail_ o _TutaNota_. 

