# Crear llistes de correu de diferents tipus

Donades les característiques d’aquesta unitat formativa ens limitarem a
verificar, per avaluar-la, que les pràctiques realitzades a classe s’han
completat.

## Exercicis

Cal verificar que aquestes llistes han estat creades i verificades en les
classes pràctiques anteriors. No cal reservar temps per fer-les _ara_.

- S’ha completat la creació d’una llista tipus _Email list_?
- S’ha completat la creació d’una llista tipus _Web forum_?
- S’ha completat la creació d’una llista tipus _Q&A forum_?
- S’ha completat la creació d’una llista tipus _Collaborative inbox_?

