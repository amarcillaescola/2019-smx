# Fulls d’estil i plantilles: conceptes bàsics

## Descripció

Es proporcionarà als alumnes un fitxer on es presenten exercicis pràctics i
escrits per ser resolts, generalment creant i editant nous fitxers.

Una vegada completats els exercicis els alumnes han de lliurar els fitxers
creats en la forma que s’els indicarà.
